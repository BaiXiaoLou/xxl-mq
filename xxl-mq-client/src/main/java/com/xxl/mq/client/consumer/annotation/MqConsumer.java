package com.xxl.mq.client.consumer.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by xuxueli on 16/8/28.
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MqConsumer {
	public static final String DEFAULT_GROUP = "DEFAULT";

	/**
	 * @return
	 */
	String group() default DEFAULT_GROUP;

	/**
	 * @return
	 */
	String topic();

	/**
	 * @return
	 */
	boolean transaction() default true;
}
