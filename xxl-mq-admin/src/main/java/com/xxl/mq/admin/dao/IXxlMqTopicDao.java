package com.xxl.mq.admin.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.xxl.mq.admin.core.model.XxlMqTopic;

/**
 * @author xuxueli 2018-11-20
 */
@Mapper
public interface IXxlMqTopicDao {
	public List<XxlMqTopic> pageList(@Param("offset") int offset, @Param("pagesize") int pagesize,
			@Param("bizId") int bizId, @Param("topic") String topic);

	public int pageListCount(@Param("offset") int offset, @Param("pagesize") int pagesize, @Param("bizId") int bizId,
			@Param("topic") String topic);

	public XxlMqTopic load(@Param("topic") String topic);

	public int add(@Param("xxlMqTopic") XxlMqTopic xxlMqTopic);

	public int update(@Param("xxlMqTopic") XxlMqTopic xxlMqTopic);

	public int delete(@Param("topic") String topic);

	public List<XxlMqTopic> findAlarmByTopic(@Param("topics") List<String> topics);
}
